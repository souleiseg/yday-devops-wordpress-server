# Yday Devops - Wordpress Server

Ce repository héberge la configuration du cluster à réaliser pour le TP Ydays - Cloud & Devops.

## Structure du projet

### kustomization.yaml
Ce fichier permet va nous permettre de déclarer nos déploiements, persistentVolumeClaim, credentials... ainsi que de mettre en place notre environnement en une seule fois.
Lors de son exécution, il ne recrée que des objets qui ont été modifiés.

### mysql.yaml
Deux objets sont définis ici : 
- **Un service** mysql écoutant sur le port 3306. 
- **Un PersistentVolumeClaim** d'une capacité maximum de 20Go en mode ReadWriteOnce.
- **Un deployment** qui s'occupe de créer un container mysql (version 5.6) écoutant sur le port 3306, avec le password root défini dans le fichier kustomization.yaml. Il permet également de monter un volume vers **/var/lib/mysql**, ainsi qu'un volume persistant. 

### wordpress.yaml
Tout comme dans le fichier **mysql.yaml**, nous avons défini ici :
- **Un service** écoutant sur le port http (80).
- **Un PersistentVolumeClaim**  d'une capacité maximum de 20Go en mode ReadWriteOnce
- **Un deployment** qui s'occupe de créer un container serveur web pour notre wordpress en lui fournissant les identifiants de notre base de données mysql, spécifiés dans notre fichier kustomization.yml. Cette partie permet également d emonter un volume vers **/var/www/html**, ainsi qu'un volume persistant

## Déployer le wordpress 

Depuis la console GCloud

- Se connecter au cluster puis saisir les commandes suivantes pour récupérer le projet et le déployer
```
git clone https://gitlab.com/souleiseg/yday-devops-wordpress-server.git
cd yday-devops-wordpress-server
$ kubectl apply -k ./
```
Pour accéder au site il est nécessaire de récupérer l'adresse IP du LoadBalancer wordpress ainsi que son port (celui à 5 chiffres, pas le port 80..):
```
$ kubectl get services wordpress
```
Pour accéder au Wordpress de notre projet, <a href="http://34.121.63.143/">cliquez ici</a>.

